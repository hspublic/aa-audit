// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract Singleton {
    // singleton slot always needs to be first declared variable, to ensure that it is at the same location as in the Proxy contract.

    /* This is the keccak-256 hash of "hs.scw.proxy.implementation" subtracted by 1 */
    bytes32 internal constant _IMPLEMENTATION_SLOT =
        0x34b5b68a907b7b471f5cb655bba6f020a316d5449006b21d667d911ae227fe3a;

    function _setImplementation(address _imp) internal {
        assert(
            _IMPLEMENTATION_SLOT == bytes32(uint256(keccak256("hs.scw.proxy.implementation")) - 1)
        );
        // solhint-disable-next-line no-inline-assembly
        assembly {
            sstore(_IMPLEMENTATION_SLOT, _imp)
        }
    }

    function _getImplementation() internal view returns (address _imp) {
        // solhint-disable-next-line no-inline-assembly
        assembly {
            _imp := sload(_IMPLEMENTATION_SLOT)
        }
    }
}
