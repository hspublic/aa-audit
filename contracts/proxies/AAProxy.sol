//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

/**
 * @title Proxy // This is the user's wallet
 * @notice Basic proxy that delegates all calls to a fixed implementing contract.
 */
contract AAProxy {
    /* This is the keccak-256 hash of "hs.scw.proxy.implementation" subtracted by 1, and is validated in the constructor */
    bytes32 internal constant _IMPLEMENTATION_SLOT =
        0x34b5b68a907b7b471f5cb655bba6f020a316d5449006b21d667d911ae227fe3a;

    event Received(uint indexed value, address indexed sender, bytes data);

    constructor(address _implementation) {
        assert(
            _IMPLEMENTATION_SLOT == bytes32(uint256(keccak256("hs.scw.proxy.implementation")) - 1)
        );
        assembly {
            sstore(_IMPLEMENTATION_SLOT, _implementation)
        }
    }

    fallback() external payable {
        address target;
        // solhint-disable-next-line no-inline-assembly
        assembly {
            target := sload(_IMPLEMENTATION_SLOT)
            calldatacopy(0, 0, calldatasize())
            let result := delegatecall(gas(), target, 0, calldatasize(), 0, 0)
            returndatacopy(0, 0, returndatasize())
            switch result
            case 0 {
                revert(0, returndatasize())
            }
            default {
                return(0, returndatasize())
            }
        }
    }

    receive() external payable {
        emit Received(msg.value, msg.sender, "");
    }
}
