//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

/**
@title SwapModuleDecoder Library that decodes arguments for swap from calldata 
@author Porco Rosso<porcorossoj89@gmail.com>
@notice this contract serves as a helper for GasPond to obtain argument values of swap functions in swapModule
*/

library TransactionDataDecoder {
    /**
    @notice this function decodes and returns argument values of functions in swapModuleUniV2.sol
    @param _calldata of swap functions such as swapETHForToken, swapTokenForETH and swapTokenForToken.
    @return tokenInAmount the input amount for swap
    @return path the swap path arrays that contains ERC20 tokenn addresses
    */
    function decodeArgs(bytes calldata _calldata) internal pure returns (bytes4, bytes memory) {
        (bytes4 functionName, bytes memory data) = extractCalldata(_calldata);
        return (functionName, data);
    }

    /**
    @notice this inner function extract calldata in a way that remove the first 4bytes(selector) from calldata
    @param _calldata of swap functions such as swapETHForToken, swapTokenForETH and swapTokenForToken.
    @return data calldata without selector and function name the first 4bytes of calldata.
    */
    function extractCalldata(
        bytes calldata _calldata
    ) internal pure returns (bytes4, bytes calldata) {
        if (_calldata.length < 4) {
            revert("INVALID_CALLDATA_LENGTH");
        }
        return (bytes4(_calldata[0:4]), _calldata[4:]);
    }
}
