// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@matterlabs/zksync-contracts/l2/system-contracts/libraries/SystemContractsCaller.sol";
import "@matterlabs/zksync-contracts/l2/system-contracts/libraries/TransactionHelper.sol";
import "./BytesLib.sol";

contract MulticallHelper {
    using TransactionHelper for Transaction;
    using BytesLib for bytes;

    // bytes4(keccak256(_executeBatchTransaction(bytes memory)))
    bytes4 public constant BATCH_TX_SELECTOR = 0x7c3068b5;

    function isBatched(bytes calldata _data, address _to) internal view returns (bool) {
        if (_data.length < 4) {
            return false;
        }
        bytes4 selector = bytes4(_data[0:4]);
        return selector == BATCH_TX_SELECTOR && _to == address(this);
    }

    function encodeBatchData(
        address[] memory targets,
        bytes[] memory methods,
        uint256[] memory values
    ) external pure returns (bytes memory _data) {
        _data = abi.encode(BATCH_TX_SELECTOR, targets, methods, values);
    }

    function _decodeBatchData(
        bytes memory _data
    )
        internal
        pure
        returns (address[] memory targets, bytes[] memory methods, uint[] memory values)
    {
        (, targets, methods, values) = abi.decode(_data, (bytes4, address[], bytes[], uint[]));

        return (targets, methods, values);
    }
}
