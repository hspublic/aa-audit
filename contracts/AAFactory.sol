// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;

import "@matterlabs/zksync-contracts/l2/system-contracts/Constants.sol";
import "@matterlabs/zksync-contracts/l2/system-contracts/libraries/SystemContractsCaller.sol";
import "./interfaces/IAccountRegistry.sol";
import "./versions/Account.sol";

contract AAFactory {
    string public constant VERSION = "1.0.3";
    bytes32 public aaBytecodeHash;
    address public immutable defaultImpl;
    address public accountRegistry;

    constructor(address _baseImpl, bytes32 _aaBytecodeHash, address _accountRegistry) {
        require(_baseImpl != address(0), "ZERO_ADDRESS_IMPL");
        defaultImpl = _baseImpl;
        aaBytecodeHash = _aaBytecodeHash;
        accountRegistry = _accountRegistry;
    }

    event SmartAccountCreated(
        address indexed proxy,
        address indexed implementation,
        address indexed owner,
        string version,
        bytes32 salt,
        uint index,
        bytes32 saltUnique
    );

    function deployAccount(
        bytes32 salt,
        address owner,
        bytes32 deviceId
    ) external returns (address proxyAccountAddress) {
        bytes32 saltUnique = keccak256(abi.encodePacked(owner, salt));
        (bool success, bytes memory returnData) = SystemContractsCaller.systemCallWithReturndata(
            uint32(gasleft()),
            address(DEPLOYER_SYSTEM_CONTRACT),
            uint128(0),
            abi.encodeCall(
                DEPLOYER_SYSTEM_CONTRACT.create2Account,
                (
                    saltUnique,
                    aaBytecodeHash,
                    abi.encode(defaultImpl),
                    IContractDeployer.AccountAbstractionVersion.Version1
                )
            )
        );
        require(success, "Deployment failed");
        (proxyAccountAddress) = abi.decode(returnData, (address));
        Account(payable(proxyAccountAddress)).init(owner);
        IAccountRegistry(accountRegistry).storeAccount(proxyAccountAddress);
        IAccountRegistry(accountRegistry).setAccountAddress(owner, salt, proxyAccountAddress);
        emit SmartAccountCreated(
            proxyAccountAddress,
            defaultImpl,
            owner,
            VERSION,
            salt,
            9999,
            saltUnique
        );
    }

    function getAccountAddress(
        address _owner,
        bytes32 _salt
    ) public view returns (address newAddress) {
        return IAccountRegistry(accountRegistry).getAccountAddress(_owner, _salt);
    }

    function getAddress(bytes32 _salt, address _owner) public view returns (address newAddress) {
        bytes32 saltUnique = keccak256(abi.encodePacked(_owner, _salt));
        bytes32 constructorInputHash = keccak256(abi.encode(defaultImpl));
        bytes32 hash = keccak256(
            bytes.concat(
                CREATE2_PREFIX,
                bytes32(uint256(uint160(address(this)))),
                saltUnique,
                aaBytecodeHash,
                constructorInputHash
            )
        );
        newAddress = address(uint160(uint256(hash)));
    }
}
