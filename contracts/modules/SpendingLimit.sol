// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

contract SpendingLimit is OwnableUpgradeable {
    uint public ONE_DAY; //for testing purposes
    address public constant NATIVE_ADDRESS = 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE;
    mapping(address => mapping(address => Limit)) public tokenLimitByUser; //userAddress=>tokenAddress=>status

    function initialize() external initializer {
        __Ownable_init();
        ONE_DAY = 10 minutes;
    }

    modifier isContract() {
        require(msg.sender.code.length != 0);
        _;
    }

    struct Limit {
        uint limit;
        uint available;
        uint resetTime;
        bool isEnabled;
    }

    struct KyberSwapDescription {
        address srcToken;
        address dstToken;
        address[] srcReceivers; // transfer src token to these addresses, default
        uint256[] srcAmounts;
        address[] feeReceivers;
        uint256[] feeAmounts;
        address dstReceiver;
        uint256 amount;
        uint256 minReturnAmount;
        uint256 flags;
        bytes permit;
    }

    struct KyberSwapExecutionParams {
        address callTarget; // call this address
        address approveTarget; // approve this address if _APPROVE_FUND set
        bytes targetData;
        KyberSwapDescription desc;
        bytes clientData;
    }

    struct SyncSwapStep {
        address pool;
        bytes data;
        address callback;
        bytes callbackData;
    }

    struct SyncSwapSwapPath {
        SyncSwapStep[] steps;
        address tokenIn;
        uint amountIn;
    }

    event LimitConfigured(address indexed user, address indexed token, uint limit);
    event LimitRemoved(address indexed user, address indexed token);

    function setTokenSpendingLimit(address _tokenAddress, uint _limit) external isContract {
        require(_limit != 0, "INVALID_LIMIT");
        uint resetTime;
        uint timestamp = block.timestamp; // L1 batch timestamp
        if (isValidUpdate(_tokenAddress)) {
            resetTime = timestamp + ONE_DAY;
        } else {
            resetTime = timestamp;
        }
        _updateLimit(_tokenAddress, _limit, _limit, resetTime, true);
        emit LimitConfigured(_msgSender(), _tokenAddress, _limit);
    }

    function removeSpendingLimit(address _token) external isContract {
        require(isValidUpdate(_token), "INVALID_UPDATE");
        _updateLimit(_token, 0, 0, 0, false);
        emit LimitRemoved(_msgSender(), _token);
    }

    function getSpendingLimitTokens(
        address _user,
        address[] memory _tokens
    ) external view returns (Limit[] memory) {
        Limit[] memory limits = new Limit[](_tokens.length);
        for (uint i; i < _tokens.length; ++i) {
            limits[i] = tokenLimitByUser[_user][_tokens[i]];
        }
        return limits;
    }

    function checkSpendingLimit(
        bytes calldata _data,
        address _target,
        uint _value
    ) external isContract {
        uint amount = 0;
        address token = _target;
        Limit memory limit;
        if (_value > 0) {
            limit = tokenLimitByUser[_msgSender()][NATIVE_ADDRESS];
            if (!limit.isEnabled) {
                return;
            }
            amount = _value;
        } else {
            if (_data.length < 4) {
                return;
            }

            bytes4 selector = bytes4(_data[0:4]);
            // transfer(address to,uint256 amount) 0xa9059cbb
            // transferFrom(address from,address to,uint256 amount) 0x23b872dd
            if (selector == 0xa9059cbb) {
                limit = tokenLimitByUser[_msgSender()][_target];
                if (!limit.isEnabled) {
                    return;
                }
                (, amount) = abi.decode(_data[4:], (address, uint256));
            } else if (selector == 0x23b872dd) {
                limit = tokenLimitByUser[_msgSender()][_target];
                if (!limit.isEnabled) {
                    return;
                }
                (, , amount) = abi.decode(_data[4:], (address, address, uint256));
            } else {
                //kyberswap swap
                if (selector == 0xe21fd0e9) {
                    KyberSwapExecutionParams memory data = abi.decode(
                        _data[4:],
                        (KyberSwapExecutionParams)
                    );
                    token = data.desc.srcToken;
                    amount = data.desc.amount;
                }
                //kyberswap swapSimpleMode
                else if (selector == 0xe21fd0e9) {
                    (, KyberSwapDescription memory data, , ) = abi.decode(
                        _data[4:],
                        (address, KyberSwapDescription, bytes, bytes)
                    );
                    token = data.srcToken;
                    amount = data.amount;
                }
                //syncswap swap
                else if (selector == 0x2cc4081e) {
                    (SyncSwapSwapPath[] memory data, , ) = abi.decode(
                        _data[4:],
                        (SyncSwapSwapPath[], uint256, uint256)
                    );
                    if (data.length > 0) {
                        token = data[0].tokenIn;
                        amount = data[0].amountIn;
                    }
                }

                limit = tokenLimitByUser[_msgSender()][token];
                if (!limit.isEnabled) {
                    return;
                }
            }
        }

        uint timestamp = block.timestamp;

        if (limit.limit != limit.available && timestamp > limit.resetTime) {
            limit.resetTime = timestamp + ONE_DAY;
            limit.available = limit.limit;

            // Or only resetTime is updated if it's the first spending after enabling limit
        } else if (limit.limit == limit.available) {
            limit.resetTime = timestamp + ONE_DAY;
        }

        // reverts if the amount exceeds the remaining available amount.
        require(limit.available >= amount, "EXCEED_DAILY_LIMIT");

        // decrement `available`
        limit.available -= amount;
        tokenLimitByUser[_msgSender()][token] = limit;
    }

    function isValidUpdate(address _token) internal view returns (bool) {
        // Reverts unless it is first spending after enabling
        // or called after 24 hours have passed since the last update.
        if (tokenLimitByUser[_msgSender()][_token].isEnabled) {
            require(
                tokenLimitByUser[_msgSender()][_token].limit ==
                    tokenLimitByUser[_msgSender()][_token].available ||
                    block.timestamp > tokenLimitByUser[_msgSender()][_token].resetTime,
                "INVALID_UPDATE"
            );
            return true;
        } else {
            return false;
        }
    }

    function _updateLimit(
        address _token,
        uint _limit,
        uint _available,
        uint _resetTime,
        bool _isEnabled
    ) private {
        Limit storage limit = tokenLimitByUser[_msgSender()][_token];
        limit.limit = _limit;
        limit.available = _available;
        limit.resetTime = _resetTime;
        limit.isEnabled = _isEnabled;
    }
}
