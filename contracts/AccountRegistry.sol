//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import "@openzeppelin/contracts/utils/introspection/IERC165.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@matterlabs/zksync-contracts/l2/system-contracts/interfaces/IAccount.sol";
import "./interfaces/IAccountRegistry.sol";

/**
@title Account Registry Contract that stoers all Account contracts deployed by AccountFactory contract
@author Porco Rosso<porcorossoj89@gmail.com>
*/

contract AccountRegistry is IAccountRegistry, OwnableUpgradeable {
    mapping(address => bool) public accounts;
    mapping(bytes32 => bool) public isDeviceRegistered;
    mapping(address => bool) public isWhitelistCaller;
    mapping(address => mapping(bytes32 => address)) public getAccountAddress;

    event AccountStored(address accountAddress);
    event DeviceStored(bytes32 deviceId);

    function initialize() external initializer {
        __Ownable_init();
    }

    function setWhitelistCaller(address _caller, bool _isAllow) external onlyOwner {
        isWhitelistCaller[_caller] = _isAllow;
    }

    /**
    @notice this function stores account address into accounts mapping and marks it as true
    @param _accountAddr account address deployed by AccountFactory
    */
    function storeAccount(address _accountAddr) external {
        require(isWhitelistCaller[_msgSender()], "PERMISSION_DENIED");
        accounts[_accountAddr] = true;
        emit AccountStored(_accountAddr);
    }

    function setAccountAddress(address _owner, bytes32 _salt, address _accountAddr) external {
        require(isWhitelistCaller[_msgSender()], "PERMISSION_DENIED");
        require(getAccountAddress[_owner][_salt] == address(0), "EXISTED_OWNER_SALT");
        getAccountAddress[_owner][_salt] = _accountAddr;
    }

    //admin can set duplicate owner+salt
    function forceSetAccountAddress(address _owner, bytes32 _salt, address _accountAddr) external {
        require(isWhitelistCaller[_msgSender()], "PERMISSION_DENIED");
        getAccountAddress[_owner][_salt] = _accountAddr;
    }

    /**
    @notice this function stores account address into accounts mapping and marks it as true
    @param _deviceId deviceId
    */
    function storeDevice(bytes32 _deviceId) external {
        require(isWhitelistCaller[_msgSender()], "PERMISSION_DENIED");
        isDeviceRegistered[_deviceId] = true;
        emit DeviceStored(_deviceId);
    }

    /**
    @notice this function returns true if an account is legit, meaning it was successfully deployed by AccountFactory
    @param _account account address
    */
    function isAccount(address _account) external view returns (bool) {
        // check if account surpports the IAccount Interface
        return
            accounts[_account] && IERC165(_account).supportsInterface(type(IAccount).interfaceId);
    }
}
