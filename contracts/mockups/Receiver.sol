// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;

contract Receiver {
    address owner;

    constructor() {
        owner = msg.sender;
    }

    function withdrawEth() external {
        require(msg.sender == owner, "DENIED");
        (bool suceess, ) = msg.sender.call{value: address(this).balance}("");
    }

    function receipt() external payable {}

    fallback() external {}

    receive() external payable {}
}
