// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;

import {AccessControl} from "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import {ERC20} from "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract MockErc20 is Ownable, ERC20, AccessControl {
    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    uint256 public freeClaim = 10000 * 1e6;

    constructor() ERC20("USDCHS", "USDCHS") {
        _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _setupRole(MINTER_ROLE, msg.sender);
        _mint(msg.sender, 1_000_000_000 * 1e6); // for AMM liquidity
    }

    function decimals() public view virtual override returns (uint8) {
        return 6;
    }

    function setFreeClaim(uint256 _freeClaim) external onlyOwner {
        freeClaim = _freeClaim;
    }

    function mint(address to, uint256 amount) external {
        _mint(to, amount);
    }

    // Get 1000 free USDC
    function getFreeUSDC() external {
        _mint(msg.sender, freeClaim);
    }
}
