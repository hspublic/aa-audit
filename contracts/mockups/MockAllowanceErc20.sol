// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract MockAllowanceErc20 {
    IERC20 public token;

    constructor(IERC20 _token) {
        token = _token;
    }

    function transferMoney(address _to, uint _amount) external {
        token.transferFrom(msg.sender, _to, _amount);
    }
}
