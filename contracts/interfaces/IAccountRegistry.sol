//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

interface IAccountRegistry {
    function storeAccount(address _accountAddr) external;

    function setAccountAddress(address _owner, bytes32 _salt, address _accountAddr) external;

    function storeDevice(bytes32 _deviceId) external;

    function getAccountAddress(address _owner, bytes32 _salt) external view returns (address);

    function isDeviceRegistered(bytes32 _deviceId) external view returns (bool);

    function isAccount(address _accountAddr) external view returns (bool);
}
