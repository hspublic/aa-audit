//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

interface ISpendingLimit {
    function checkSpendingLimit(bytes calldata _data, address _target, uint _value) external;
}
