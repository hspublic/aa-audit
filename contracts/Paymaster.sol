// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

import {IPaymaster, ExecutionResult, PAYMASTER_VALIDATION_SUCCESS_MAGIC} from "@matterlabs/zksync-contracts/l2/system-contracts/interfaces/IPaymaster.sol";
import {IPaymasterFlow} from "@matterlabs/zksync-contracts/l2/system-contracts/interfaces/IPaymasterFlow.sol";
import {TransactionHelper, Transaction} from "@matterlabs/zksync-contracts/l2/system-contracts/libraries/TransactionHelper.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@matterlabs/zksync-contracts/l2/system-contracts/Constants.sol";
import "./interfaces/IAccountRegistry.sol";
import {TransactionDataDecoder} from "./libs/TransactionDataDecoder.sol";

contract Paymaster is IPaymaster {
    IAccountRegistry public accountRegistry;
    using Counters for Counters.Counter;
    Counters.Counter private countSponsor;
    uint public maxSponsor;
    address public owner;
    mapping(address => mapping(bytes4 => bool)) public isSponsorGas;
    modifier onlyBootloader() {
        require(msg.sender == BOOTLOADER_FORMAL_ADDRESS, "Only bootloader can call this method");
        // Continue execution if called from the bootloader.
        _;
    }
    modifier onlyOwner() {
        require(msg.sender == owner, "NOT_OWNER");
        _;
    }

    constructor(IAccountRegistry _accountRegistry) {
        owner = msg.sender;
        accountRegistry = _accountRegistry;
        maxSponsor = 1000;
    }

    function setMaxSponsor(uint _maxSponsor) external onlyOwner {
        maxSponsor = _maxSponsor;
    }

    function setAccountRegistry(IAccountRegistry _accountRegistry) external onlyOwner {
        accountRegistry = _accountRegistry;
    }

    function whitelistSponsor(
        address _contract,
        bytes4 _selector,
        bool _status
    ) external onlyOwner {
        isSponsorGas[_contract][_selector] = _status;
    }

    function validateAndPayForPaymasterTransaction(
        bytes32,
        bytes32,
        Transaction calldata _transaction
    ) external payable onlyBootloader returns (bytes4 magic, bytes memory context) {
        // By default we consider the transaction as accepted.
        magic = PAYMASTER_VALIDATION_SUCCESS_MAGIC;
        require(
            _transaction.paymasterInput.length >= 4,
            "The standard paymaster input must be at least 4 bytes long"
        );
        uint256 ethFee = _transaction.gasLimit * _transaction.maxFeePerGas;
        require(address(this).balance >= ethFee, "INSUFFICIENT_ETH_TO_PAY_FEE");
        bytes4 paymasterInputSelector = bytes4(_transaction.paymasterInput[0:4]);
        if (paymasterInputSelector == IPaymasterFlow.approvalBased.selector) {
            revert("Unsupported paymaster flow");
        } else {
            _approvalGeneralFlow(_transaction);
        }
        _payErgs(ethFee);
    }

    /**
    @notice funciton to pay the gas fee to BOOTLOADER_FORMAL_ADDRESS on behalf of the user.
    @param _ethFee the amount of the gas fee paid to Bootloader.
     */
    function _payErgs(uint256 _ethFee) internal {
        (bool success, ) = payable(BOOTLOADER_FORMAL_ADDRESS).call{value: _ethFee}("");
        require(success, "GAS_PAYMENT_FAILED");
    }

    /**
    @notice this innner function carries out validations for transactions with approvalBasedFlow. 
    @notice Validation likely fails unless the specified sponsor has enabled gas-payment and configured values in the ERC20Payment struct correctly. 
    @param _transaction the transaction struct defined by Transaction struct in TransactionHelper.
     */
    function _approvalGeneralFlow(Transaction calldata _transaction) internal view {
        address from = address(uint160(_transaction.from));
        address to = address(uint160(_transaction.to));

        (bytes4 functionName, bytes32 salt, , bytes32 deviceId) = _decodeCallData(
            _transaction.data
        );
        //check device exists
        require(countSponsor.current() < maxSponsor, "NO_MORE_SPONSORSHIP");
        require(isSponsorGas[to][functionName], "NOT_SPONSOR");
        require(_isDeviceNotExist(salt, deviceId), "DEVICE_WAS_SPONSORED");
    }

    function _isDeviceNotExist(bytes32 _salt, bytes32 _deviceId) internal view returns (bool) {
        return
            (address(accountRegistry) == address(0) ||
                !accountRegistry.isDeviceRegistered(_deviceId)) && _salt == bytes32(0);
    }

    function postTransaction(
        bytes calldata _context,
        Transaction calldata _transaction,
        bytes32,
        bytes32,
        ExecutionResult _txResult,
        uint256 _maxRefundedGas
    ) external payable override onlyBootloader {
        // Refunds are not supported yet.
        if (_txResult == ExecutionResult.Success) {
            countSponsor.increment();
            _storeDeviceId(_transaction.data);
        }
    }

    function _decodeCallData(
        bytes calldata _calldata
    ) internal view returns (bytes4, bytes32, address, bytes32) {
        (bytes4 functionName, bytes memory callDataParams) = TransactionDataDecoder.decodeArgs(
            _calldata
        );
        (bytes32 salt, address owner, bytes32 deviceId) = abi.decode(
            callDataParams,
            (bytes32, address, bytes32)
        );
        return (functionName, salt, owner, deviceId);
    }

    function _storeDeviceId(bytes calldata _calldata) internal {
        (, , , bytes32 deviceId) = _decodeCallData(_calldata);
        accountRegistry.storeDevice(deviceId);
    }

    function totalSponsored() external view returns (uint) {
        return countSponsor.current();
    }

    /**
    @notice this function allows sponsors to withdraw thier deposited ETH
    */
    function withdrawETH() external onlyOwner {
        withdrawETH(address(this).balance);
    }

    /**
    @notice this function allows sponsors to withdraw thier deposited ETH
    @param _amount the amount of eth withdrawn
    */
    function withdrawETH(uint256 _amount) public onlyOwner {
        require(_amount != 0, "INVALID_AMOUNT");
        (bool suceess, ) = msg.sender.call{value: _amount}("");
        if (!suceess) revert("WITHDRAWAL_FAILED");
    }

    /**
    @notice this function allows sponsors to withdraw ERC20 they can claim
    @param _token erc20 token
    */
    function withdrawToken(address _token) external onlyOwner {
        withdrawToken(_token, IERC20(_token).balanceOf(address(this)));
    }

    /**
    @notice this function allows sponsors to withdraw ERC20 they can claim
    @param _token erc20 token
    @param _amount the amount of eth withdrawn
    */
    function withdrawToken(address _token, uint256 _amount) public onlyOwner {
        require(_amount != 0, "INVALID_AMOUNT");
        IERC20(_token).transfer(msg.sender, _amount);
    }

    function depositETH() public payable returns (uint256) {
        return msg.value;
    }

    receive() external payable {
        depositETH();
    }
}
